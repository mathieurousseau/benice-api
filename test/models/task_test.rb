require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  test "Should not save incomplete task" do
    task = Task.new
    assert_not task.save
  end

  test "Save Task" do
    task = Task.new
    task.name ="My task"
    task.points = 1
    task.family = families(:family1)
    assert task.save
  end
end
