require 'test_helper'

class FamilyTest < ActiveSupport::TestCase
  test "should not save incomplete family" do
    family = Family.new
    assert_not family.save
  end

  test "Family member count" do
    assert_equal families(:family1).members.count, 3
  end
end
