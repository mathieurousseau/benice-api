require 'test_helper'

class ConsequenceTest < ActiveSupport::TestCase
  test "Should not save incomplete consequence" do
    consequence = Consequence.new
    assert_not consequence.save
  end

  test "Save Consequence" do
    consequence = Consequence.new
    consequence.name ="My consequence"
    consequence.points = 1
    consequence.family = families(:family1)
    assert consequence.save
  end
end
