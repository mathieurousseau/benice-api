require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest

  setup do
    @owner_1_token = log_in users(:owner_1)
    @owner_2_token = log_in users(:owner_2)
  end

  test "Get my family's tasks" do
    get api_v1_tasks_url(users(:owner_1).family.id)
    assert_response :unauthorized
    get api_v1_tasks_url, headers: @owner_1_token
    assert_response :success
  end

  test "Create new task" do
    before = Task.all.count
    
    # No good parameters
    post api_v1_tasks_url, params: { other: 'New task name' }, headers: @owner_1_token
    assert_response :bad_request
    assert_includes json_response['error'], 'name'

    # Only name ok
    post api_v1_tasks_url, params: { name: 'New task name' }, headers: @owner_1_token
    assert_response :bad_request
    assert_includes json_response['error'], 'points'

    post api_v1_tasks_url, params: { name: 'New task name', points: 1 }, headers: @owner_1_token
    assert_response :success

    assert_equal before + 1, Task.all.count
  end

  test "Update task" do
    @task = users(:owner_1).family.tasks.first
    put api_v1_task_url(@task.id), headers: @owner_1_token
    assert_response :bad_request

    put api_v1_task_url(@task.id), params: {name: @task.name, points: @task.points}, headers: @owner_1_token
    assert_response :success
    assert_equal @task, Task.find(@task.id)

    put api_v1_task_url(@task.id), params: {name: @task.name, points: 2}, headers: @owner_1_token
    assert_response :success
    assert_not_equal @task.points, Task.find(@task.id).points

    put api_v1_task_url(@task.id), headers: @owner_2_token
    assert_response :forbidden
  end
end
