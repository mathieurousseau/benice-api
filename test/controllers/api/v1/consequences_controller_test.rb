require 'test_helper'

class ConsequencesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @owner_1_token = log_in users(:owner_1)
    @owner_2_token = log_in users(:owner_2)
  end

  test "Get my family's consequences" do
    get api_v1_consequences_url(users(:owner_1).family.id)
    assert_response :unauthorized
    get api_v1_consequences_url, headers: @owner_1_token
    assert_response :success
  end

  test "Create new consequence" do
    before = Consequence.all.count
    
    # No good parameters
    post api_v1_consequences_url, params: { other: 'New consequence name' }, headers: @owner_1_token
    assert_response :bad_request
    assert_includes json_response['error'], 'name'

    # Only name ok
    post api_v1_consequences_url, params: { name: 'New consequence name' }, headers: @owner_1_token
    assert_response :bad_request
    assert_includes json_response['error'], 'points'

    post api_v1_consequences_url, params: { name: 'New consequence name', points: 1 }, headers: @owner_1_token
    assert_response :success

    assert_equal before + 1, Consequence.all.count
  end

  test "Update consequence" do
    @consequence = users(:owner_1).family.consequences.first
    put api_v1_consequence_url(@consequence.id), headers: @owner_1_token
    assert_response :bad_request

    put api_v1_consequence_url(@consequence.id), params: {name: @consequence.name, points: @consequence.points}, headers: @owner_1_token
    assert_response :success
    assert_equal @consequence, Consequence.find(@consequence.id)

    put api_v1_consequence_url(@consequence.id), params: {name: @consequence.name, points: 2}, headers: @owner_1_token
    assert_response :success
    assert_not_equal @consequence.points, Consequence.find(@consequence.id).points

    put api_v1_consequence_url(@consequence.id), headers: @owner_2_token
    assert_response :forbidden
  end
end
