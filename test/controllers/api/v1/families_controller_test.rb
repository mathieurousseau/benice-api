require 'test_helper'

class Api::V1::FamiliesControllerTest < ActionDispatch::IntegrationTest


  setup do
    @owner_1_token = log_in users(:owner_1)
    @owner_2_token = log_in users(:owner_2)
  end

  test "index forbidden" do
    get api_v1_families_url, headers: @owner_1_token
    assert_response :forbidden
  end

  test "Should be authenticated" do
    get api_v1_families_url
    assert_response :unauthorized
  end
  

  test "Show my family" do
    get api_v1_family_url(users(:owner_1).family.id), headers: @owner_1_token
    assert_response :success
    get api_v1_family_url(users(:owner_2).family.id), headers: @owner_2_token
    assert_response :success
    get api_v1_family_url(users(:member_1_1).family.id), headers: @owner_1_token
    assert_response :success
    get api_v1_family_url(users(:member_1_2).family.id), headers: @owner_1_token
    assert_response :forbidden
  end

  test "Show other's family" do
    get api_v1_family_url(users(:owner_2).family.id), headers: @owner_1_token
    assert_response :forbidden
  end

end
