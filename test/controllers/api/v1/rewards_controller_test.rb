require 'test_helper'

class RewardsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @owner_1_token = log_in users(:owner_1)
    @owner_2_token = log_in users(:owner_2)
  end

  test "Get my family's rewards" do
    get api_v1_rewards_url(users(:owner_1).family.id)
    assert_response :unauthorized
    get api_v1_rewards_url, headers: @owner_1_token
    assert_response :success
  end

  test "Create new reward" do
    before = Reward.all.count
    
    # No good parameters
    post api_v1_rewards_url, params: { other: 'New reward name' }, headers: @owner_1_token
    assert_response :bad_request
    assert_includes json_response['error'], 'name'

    # Only name ok
    post api_v1_rewards_url, params: { name: 'New reward name' }, headers: @owner_1_token
    assert_response :bad_request
    assert_includes json_response['error'], 'points'

    post api_v1_rewards_url, params: { name: 'New reward name', points: 1 }, headers: @owner_1_token
    assert_response :success

    assert_equal before + 1, Reward.all.count
  end

  test "Update reward" do
    @reward = users(:owner_1).family.rewards.first
    put api_v1_reward_url(@reward.id), headers: @owner_1_token
    assert_response :bad_request

    put api_v1_reward_url(@reward.id), params: {name: @reward.name, points: @reward.points}, headers: @owner_1_token
    assert_response :success
    assert_equal @reward, Reward.find(@reward.id)

    put api_v1_reward_url(@reward.id), params: {name: @reward.name, points: 2}, headers: @owner_1_token
    assert_response :success
    assert_not_equal @reward.points, Reward.find(@reward.id).points

    put api_v1_reward_url(@reward.id), headers: @owner_2_token
    assert_response :forbidden
  end
end
