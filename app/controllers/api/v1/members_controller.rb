module Api::V1
  class MembersController < ApiController
    before_action :authenticate_user!, except: %i[index show]
    # before_action :set_family, only: %i[show edit update destroy]
    # GET /members
    def index
      @members = current_user.family.members
      json_response(@members)
    end

    private

  end
end
