module Api::V1
  class TasksController < ApiController
    before_action :authenticate_user!
    before_action :set_task, only: %i[show edit update destroy]
    before_action :check_ownership, only: %i[show edit update destroy]

    # GET /tasks
    def index
      json_response Task.find_by family: current_user.family
    end

    # POST /task
    def create
      @task = Task.new(task_params)
      @task.family = current_user.family
      if @task.save
        json_response(@task, :created)  
      else
        json_response @task, :error
      end
    end

    # PUT /task
    def update
      @task.update(task_params)
    end

    private

    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require([:name, :points])
      params.permit([:name, :points, :description, :icon])
    end
    
    def check_ownership
      if !current_user.family.tasks.include? @task
        head :forbidden
      end
    end

  end
end
