module Api::V1
  class ConsequencesController < ApiController
    before_action :authenticate_user!
    before_action :set_consequence, only: %i[show edit update destroy]
    before_action :check_ownership, only: %i[show edit update destroy]
    
    # GET /consequences
    def index
      json_response Consequence.find_by family: current_user.family
    end

    # POST /consequence
    def create
      @consequence = Consequence.new(consequence_params)
      @consequence.family = current_user.family
      if @consequence.save
        json_response(@consequence, :created)  
      else
        json_response @consequence, :error
      end
    end

    # PUT /consequence
    def update
      @consequence.update(consequence_params)
    end

    private

    def set_consequence
      @consequence = Consequence.find(params[:id])
    end

    def consequence_params
      params.require([:name, :points])
      params.permit([:name, :points, :description, :icon])
    end
    
    def check_ownership
      if !current_user.family.consequences.include? @consequence
        head :forbidden
      end
    end
    
  end
end
