module Api::V1
    class FamiliesController < ApiController
        before_action :authenticate_user!
        before_action :set_family, only: %i[show edit update destroy]

        # GET /families
        def index
          return head :forbidden
        end
        
        # GET /family/:id
        def show
          if @family.id.to_s == params[:id]
            json_response(@family)
          else
            head :forbidden
          end
        end

        private

        def family_params
            params.permit(:name, :description)
        end

        def set_family
            @family = Family.find(current_user.family.id)
        end
    end
end
