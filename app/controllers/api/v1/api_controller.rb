# frozen_string_literal: true

module Api::V1
  class ApiController < ApplicationController
    # include OpenApi::DSL
    include DeviseTokenAuth::Concerns::SetUserByToken
    include Response
    include ExceptionHandler
    
  end
end
  