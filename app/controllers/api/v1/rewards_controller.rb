module Api::V1
  class RewardsController < ApiController
    before_action :authenticate_user!
    before_action :set_reward, only: %i[show edit update destroy]
    before_action :check_ownership, only: %i[show edit update destroy]
    
    # GET /rewards
    def index
      json_response Reward.find_by family: current_user.family
    end

    # POST /reward
    def create
      @reward = Reward.new(reward_params)
      @reward.family = current_user.family
      if @reward.save
        json_response(@reward, :created)  
      else
        json_response @reward, :error
      end
    end

    # PUT /reward
    def update
      @reward.update(reward_params)
    end

    private

    def set_reward
      @reward = Reward.find(params[:id])
    end

    def reward_params
      params.require([:name, :points])
      params.permit([:name, :points, :description, :icon])
    end
    
    def check_ownership
      if !current_user.family.rewards.include? @reward
        head :forbidden
      end
    end
  end
end
