# frozen_string_literal: true

module Response
  def json_response(object, status = :ok)
    _class_name = object.is_a?(ActiveRecord::Relation) ? object.klass.name : object.class.name
    render json: object, status: status, \
      :include => Object.const_get("#{_class_name}Serializer")::INCLUDE
  end
end