# frozen_string_literal: true

class User < ActiveRecord::Base
  belongs_to :family
  before_validation :build_default_family
  # validates :name, presence: true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable
  include DeviseTokenAuth::Concerns::User

  private
  def build_default_family
    if self.family.nil?
      self.family = Family.create({:name => "#{name}'s Family'", :description => "Enter your family's description" })
    end
    # build_profile
    true # Always return true in callbacks as the normal 'continue' state
        # Assumes that the default_profile can **always** be created.
        # or
        # Check the validation of the profile. If it is not valid, then
        # return false from the callback. Best to use a before_validation 
        # if doing this. View code should check the errors of the child.
        # Or add the child's errors to the User model's error array of the :base
        # error item
  end
  
end
