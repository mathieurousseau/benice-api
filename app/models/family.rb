class Family < ApplicationRecord
    has_many :members, class_name: "User",
                          foreign_key: "family_id"
    has_many :tasks
    has_many :consequences
    has_many :rewards
    validates :name, presence: true
end
