class Reward < ApplicationRecord
  belongs_to :family
  validates :name, :points, presence: true
end
