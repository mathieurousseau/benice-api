# A sample Guardfile
# More info at https://github.com/guard/guard#readme

## Uncomment and set this to only include directories you want to watch
# directories %w(app lib config test spec features) \
#  .select{|d| Dir.exist?(d) ? d : UI.warning("Directory #{d} does not exist")}

## Note: if you are using the `directories` clause above and you are not
## watching the project directory ('.'), then you will want to move
## the Guardfile to a watched dir and symlink it back, e.g.
#
#  $ mkdir config
#  $ mv Guardfile config/
#  $ ln -s config/Guardfile .
#
# and, you'll have to watch "config/Guardfile" instead of "Guardfile"

require 'socket'

if "mac" == ENV['DOCKER_DEV_OS']
  grown_host = 'host.docker.internal'
else
  grown_host = Socket.ip_address_list.detect{|intf| intf.ipv4_private?}.ip_address.gsub(/([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)[0-9]{1,3}$/,'\11')
end

notification :gntp, :host => grown_host, :password => ENV['GROWL_PWD']


guard :minitest, spring: "bin/rails test" do
  # with Minitest::Unit
  watch(%r{^test/(.*)\/?test_(.*)\.rb$})
  watch(%r{^test/controllers\/(.*)\/(.*)\/?(.*)_test\.rb$})
  watch(%r{^test/models\/?(.*)_test\.rb$})
  # watch(%r{^app\/controllers\/(.*)\/(.*)\/?(.*)_controller\.rb$})
  watch(%r{^lib/(.*/)?([^/]+)\.rb$})     { |m| "test/#{m[1]}test_#{m[2]}.rb" }
  watch(%r{^test/test_helper\.rb$})      { 'test' }
  watch(%r{^test/fixtures/(.+)\.yml$}) { 'test'}


  watch(%r{^config/routes.rb$}) { 'test'}

  # with Minitest::Spec
  # watch(%r{^spec/(.*)_spec\.rb$})
  # watch(%r{^lib/(.+)\.rb$})         { |m| "spec/#{m[1]}_spec.rb" }
  # watch(%r{^spec/spec_helper\.rb$}) { 'spec' }

  # Rails 4
  # watch(%r{^app/(.+)\.rb$})                               { |m| "test/#{m[1]}_test.rb" }
  watch(%r{^app/controllers/application_controller\.rb$}) { 'test/controllers' }
  # watch(%r{^app/controllers/(.+)_controller\.rb$})        { |m| "test/integration/#{m[1]}_test.rb" }
  watch(%r{^app/controllers/(.*)/(.*)/(.+)_controller\.rb$})        { |m| "test/controllers/#{m[1]}/#{m[2]}/#{m[3]}_controller_test.rb" }
  watch(%r{^app/models/(.+)\.rb$})        { |m| "test/models/#{m[1]}_test.rb" }
  # watch(%r{^app/controllers\/(.*)\/(.*)\/?(.*)_controller\.rb$})        { 'test/controllers' }
  # watch(%r{^app/views/(.+)_mailer/.+})                    { |m| "test/mailers/#{m[1]}_mailer_test.rb" }
  # watch(%r{^lib/(.+)\.rb$})                               { |m| "test/lib/#{m[1]}_test.rb" }
  # watch(%r{^test/.+_test\.rb$})
  # watch(%r{^test/test_helper\.rb$}) { 'test' }

  # Rails < 4
  # watch(%r{^app/controllers/(.*)\.rb$}) { |m| "test/functional/#{m[1]}_test.rb" }
  # watch(%r{^app/helpers/(.*)\.rb$})     { |m| "test/helpers/#{m[1]}_test.rb" }
  # watch(%r{^app/models/(.*)\.rb$})      { |m| "test/unit/#{m[1]}_test.rb" }
end
