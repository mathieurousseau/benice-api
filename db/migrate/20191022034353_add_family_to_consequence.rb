class AddFamilyToConsequence < ActiveRecord::Migration[6.0]
  def change
    add_reference :consequences, :family, null: false, foreign_key: true
  end
end
