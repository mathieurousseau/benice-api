class CreateConsequences < ActiveRecord::Migration[6.0]
  def change
    create_table :consequences do |t|
      t.string :name
      t.text :description
      t.integer :points
      t.string :icon

      t.timestamps
    end
  end
end
